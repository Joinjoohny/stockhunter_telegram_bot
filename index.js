'use strict';
// Lib
const express = require('express');
const app = express();
const chalk = require('chalk');
// Modules
const amazonTracker = require('./tracker');
const db = require('./src/db');
const log = console.log;
amazonTracker.initWorker();
// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


// When successfully connected
db.connection.on('connected', () => {
  log(chalk.green(`Mongoose default connection open to ${db.connection.host} db name '${db.connection.name}'`));
});

// If the connection throws an error
db.connection.on('error', err => {
  log(chalk.red('Mongoose default connection error: ' + err));
});

// When the connection is disconnected
db.connection.on('disconnected', () => {
  log(chalk.red('Mongoose default connection disconnected'));
});


module.exports = app;
