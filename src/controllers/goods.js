'use strict';

const Goods = require('../models/goods');

module.exports = {
  saveGoods: (userId, url, productId) => (Goods.create({ userId, url, productId })),
  getAllGoods: () => (Goods.find({})),
  removeGoods: (userId, productId) => (Goods.remove({ userId, productId })),
};
