'use strict';
/**
 * Schema Definitions
 *
 */
const mongoose = require('mongoose');

const GoodsSchema = new mongoose.Schema({
  userId: Number,
  productId: String,
  url: String,
}, { timestamps: true });

module.exports = mongoose.model('goods', GoodsSchema);
