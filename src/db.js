'use strict';

const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
// Url mongodb
const url = 'mongodb://127.0.0.1/trackerBot';
// Connect
mongoose.connect(url);

module.exports = mongoose;
