'use strict';

// Main
const TelegramBot = require('node-telegram-bot-api');
const token = '454476959:AAHjyEEpsq6v6dUjConeN9IUsS4QzhrQLSY';
const bot = new TelegramBot(token, {polling: true});
// Help
const chalk = require('chalk');
const log = console.log;
// Modules
const { proccessUrl } = require('../tracker/helpers');
const { saveGoods, removeGoods } = require('../src/controllers/goods');

bot.on('message', msg => {
  // const chatId = msg.chat.id;
});

bot.on('polling_error', err => {
  log(chalk.red(err.code, 'polling_error'));
});

bot.on('callback_query', value => {
  const data = value.data.split('_');
  const { id } = value.from;

  switch (data[0]) {
    case 'stopTracking':
      const productId = data[1];
      removeGoods(id, productId).then(value => {
        if (value.n > 0) {
          log(chalk.yellow(value, `Product ${productId} was removed`));
          bot.sendMessage(id, 'Product was removed ✅');
        }
      });
      break;
  }
});

bot.onText(/\/start/, (msg, match) => {
  const chatId = msg.chat.id;

  bot.sendMessage(chatId, 'Hi ✌️ I\'m amazon tracker Bot. Use command /add for adding new product 📦');
});

// add [url]
bot.onText(/\/add (.+)/, (msg, match) => {
  const chatId = msg.chat.id;
  const trackUrl = match[1];

  if (trackUrl && proccessUrl(trackUrl)) {
    const productId = proccessUrl(trackUrl);

    saveGoods(chatId, trackUrl, productId).then(value => {
      log(chalk.yellow(value, 'Product was added'));
      bot.sendMessage(chatId, 'Product was added ✅');
    }, err => console.log(err));
  } else {
    bot.sendMessage(chatId, 'Please add correct product url ⛔');
  }

});

const sendMessage = ({ userId, url, productId }) => {
  const options = {
    parse_mode: 'Markdown',
    reply_markup: {
      inline_keyboard: [
        [{
          text: '❌ Stop tracking',
          callback_data: `stopTracking_${productId}`
        }]
      ]
    }
  };

  bot.sendMessage(userId, `🛍️ Product *in stock* 👉 ${url}`, options);
};

module.exports = { sendMessage };
