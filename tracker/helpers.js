'use strict';

const proccessUrl = trackUrl => {
  const isAmazonUrl = [];
  let productId = null;
  const partsUrl = trackUrl.split('/');

  partsUrl.forEach((item, index) => {
    if (item.includes('dp')) {
      productId = partsUrl[index + 1];
    }

    if (item.includes('www.amazon.com') || item.includes('dp')) isAmazonUrl.push(item);
  });

  // Check url 2 arguments
  return (isAmazonUrl.length === 2) ? productId : false;
};

module.exports = { proccessUrl };
