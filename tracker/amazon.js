'use strict';

const
  cheerio = require('cheerio'),
  async = require('async'),
  chalk = require('chalk'),
  driver = require('node-phantom-simple'),
  bot = require('../bot/telegram');

const log = console.log;
// Modules
const { getAllGoods } = require('../src/controllers/goods');

const runTracker = () => {
  log(chalk.yellow('runTracker started.'));

  driver.create({ path: require('phantomjs').path }, (err, browser) => {

    getAllGoods().then(items => {
      async.each(items, (item, cb) => {

        if (err) cb(err);

        browser.createPage((err, page) => {
          page.open(item.url, (err, status) => {
            page.get('content', (err, html) => {

              const $ = cheerio.load(html);
              const str = $('span.a-size-medium.a-color-success').text();
              const captcha = $('p.a-last').length;
              const stock = (str.indexOf('Available from') !== -1);

              if (captcha) {
                log(chalk.red(`${item.productId} - captcha >`));
                console.log($('p.a-last').text());
              } else if (!stock) {
                bot.sendMessage(item);
                log(chalk.green(`${item.productId} in stock`));
              }

              cb();

            });

          });

        });

      }, err => {
        if (err) {
          console.log('A products failed to process');
        } else {
          console.log('All products have been processed successfully');
        }
      });
    });
  });
};

module.exports = { runTracker };
