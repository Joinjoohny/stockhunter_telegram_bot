'use strict';

// Lib
const chalk = require('chalk');
const CronJob = require('cron').CronJob;
// Modules
const amazonTracker = require('./amazon');

const initWorker = () => {
  console.log(chalk.yellow('Init worker started NOW.'));
  // amazonTracker.runTracker();
  // Run jobs
  // Run cron job every 5 min
  new CronJob('0 */5 * * * *', () => {
    console.log(chalk.yellow('Amazon Tracker Cron Job started NOW.'));
    amazonTracker.runTracker();
  }, null, true, 'Europe/Kiev');
};

module.exports = { initWorker };
